# Filebackup
This is an example module that shows a bare minimum way to backup files in FreePBX® 15 originally written by James Finstrom

These examples shows how to backup multiple directories and file types. They have been tested to prove they Backup the files and restore them. 


## Introduction

In backup 15 the modules control what they need to backup and restore. There is no method of adding extra files to the backup.
This module demonstrates a way to add needed files to a skeleton module to add them to the backup.
It probably makes more sense to use a database and UI to make this more manageable. This is not meant to be a "complete tool" but more as a demo.

## License
AGPLv3 https://www.gnu.org/licenses/agpl-3.0.en.html

## Disclaimers
This is provided without warranty etc...
This is not an official project of Sangoma or FreePBX
FreePBX is a trademark of Sangoma and is not used to imply association with or support of the trademark holder.

The original Git repository is at https://github.com/jfinstrom/filebackup 

Enjoy :-)